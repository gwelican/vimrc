syntax on 

" Sets how many lines of history VIM has to remember
set history=700

" Be smart when using tabs ;)
set smarttab

" Use spaces instead of tabs
set expandtab

" 1 tab == 2 spaces
set shiftwidth=2
set tabstop=2

set ai "Auto indent
set si "Smart indent
set nocompatible

" Show matching brackets when text indicator is over them
set showmatch 

"Always show current position
set ruler 

" Makes search act like search in modern browsers
set incsearch


" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

" Enable filetype plugins
filetype plugin on
filetype indent on


let perl_extended_vars=1
let mapleader = ","
map <leader>tt :tabnew<cr>
map <leader>tn :tabnext<cr>
map <leader>tp :tabprevious<cr>
map <leader>tm :tabmove

set nu
set foldenable
set foldmethod=marker
set foldlevel=0
" How many tenths of a second to blink when matching brackets
set mat=5

" Set 7 lines to the cursor - when moving vertically using j/k
set so=7

" Don't redraw while executing macros (good performance config)
set lazyredraw

" For regular expressions turn magic on
set magic

" Set utf8 as standard encoding and en_US as the standard language
set encoding=utf8

" Use Unix as the standard file type
set ffs=unix,dos,mac


set nocursorline

set mouse=a
set ttymouse=xterm

set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*
let g:syntastic_enable_signs=1
let g:syntastic_quiet_warnings=1
let g:syntastic_auto_loc_list=1

au! BufRead,BufNewFile *.pp setfiletype puppet
au! BufRead,BufNewfile *.pp set filetype=puppet


colorscheme ir_black

runtime macros/matchit.vim
execute pathogen#infect()

" remember the last position
if has("autocmd")
  au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exe "normal! g'\"" | endif
endif

